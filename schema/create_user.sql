CREATE TABLE users(  
    username        varchar(50) NOT NULL comment 'primary key',
    hash_password   varchar(2048) NOT NULL COMMENT 'null for the moment',
    email           varchar(255) COMMENT 'null for the moment',
    is_active       varchar(3) NOT NULL COMMENT 'user is active',
    last_login      DATETIME null COMMENT 'last_login',
    last_logout     DATETIME null COMMENT 'last_logout',
    constraint USERS_PK     PRIMARY KEY (username)
) default charset utf8 comment '';
