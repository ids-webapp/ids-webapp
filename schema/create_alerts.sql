CREATE TABLE alerts(  
    alert_id            BIGINT NOT NULL AUTO_INCREMENT comment 'primary key',
    protocol            char(5) not null,
    src_ip              char(15) null,
    dest_ip             char(15) null,
    alert_datetime      DATETIME null COMMENT 'arrival_datetime',
    classification      varchar(255) not null,
    direction           char(8) null COMMENT 'null for the moment',
    constraint ALERTS_PK   PRIMARY KEY (alert_id)
) default charset utf8 comment '';