CREATE TABLE filters(  
    filter_id           BIGINT NOT NULL AUTO_INCREMENT comment 'primary key',
    filter_protocol     CHAR(10) not null,
    filter_args         varchar(2048) null comment 'null for the moment',
    is_enabled          char(3) not null,
    modified_by         varchar(50) null COMMENT 'modified_by null for the moment',
    datetime_modified   DATETIME null COMMENT 'datetime_modified',
    constraint FILTERS_PK   PRIMARY KEY (filter_id)
) default charset utf8 comment '';