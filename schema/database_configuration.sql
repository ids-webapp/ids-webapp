/* 
Ensure you're in the same location where schema folder is located.
schema folder can be found in 'ids-webapp/schema'
*/

-- Configuring database

-- Create table users
source create_user.sql

-- Create table filters
source create_filters.sql

-- Create table alerts
source create_alerts.sql

-- Create table packets
source create_packets.sql

-- Restore dummy data
source pkts_alerts_backup.sql

-- Database configuration completed.
