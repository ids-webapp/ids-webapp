CREATE TABLE packets(  
    packet_id           BIGINT NOT NULL AUTO_INCREMENT comment 'primary key',
    filter_id           BIGINT NULL comment 'null for the moment',
    interface            varchar(50) null,
    frame_time          varchar(20) null comment 'null for the moment',
    src_ip              char(15) null,
    src_port            char(5) null,
    dest_ip             char(15) null,
    dest_port           char(5) null,
    protocol            char(5) not null,
    packet_length       varchar(9999) null comment 'null for the moment',
    classification      varchar(255) null comment 'null for the moment',
    arrival_datetime    DATETIME null COMMENT 'arrival_datetime',
    direction           char(8) null COMMENT 'packet direction null for the moment',
    alert_id 		 	BIGINT NULL,
    constraint PACKETS_PK   PRIMARY KEY (packet_id)
) default charset utf8 comment '';
