# System libraries
from threading import Thread
from subprocess import Popen, PIPE
from pathlib import Path
from functools import partial
from itertools import islice
# from typing import List
from datetime import datetime
import decimal
import os
import signal
import time
import re
import pickle
import pprint as pp

# Third-party libraries
# import pyshark as shark
import pandas as pd
import numpy as np
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import Pipeline
import mysql.connector
import pytz
from scapy.all import *
from scapy.layers.l2 import Ether

# My libraries
from .db_conn import open_conn
from .signature import Signature


class Sniffer:
    # Constants
    PATHS = {'model_path': './fyp_pythonflask/models/knn_model.pkl',
             'preprocess_script_path': None,
             'initial_pcap_save_path': './fyp_pythonflask/initial_save/',
             'final_pcap_save_path': './fyp_pythonflask/final_save/',
             'alert_log': './fyp_pythonflask/alert_logs/',
             'file_alert_log': 'fyp_pythonflask/file_alert_logs/',
             'split_path': 'fyp_pythonflask/split/',
             'process_script_path': 'fyp_pythonflask/models/'}
    
    ATTACKS = ['Normal', 'DoS', 'Probe', 'U2R', 'R2L']

    # Status tracking (protected class variable)
    _status = {'live_capture_status': True,
               'live_processing_status': False,
               'live_update_status': False,
               'live_alert_status': False,
               'live_classification_status': False,
               'file_capture_status': False,
               'file_processing_status': False,
               'file_update_status': False,
               'file_classification_status': False,
               'store_packets_to_db_status': False,
               'store_events_to_db_status': False,
               'store_alerts_to_db_status': False}

    # Live packets tracking
    live_packets = {'packets': [],
                    'bad_packets': [],
                    'good_packets': [],
                    'prediction': [],
                    'previous_packets': [],
                    'store': [],
                    'alerts': []}

    # File packets tracking
    file_packets = {'packets': [],
                    'bad_packets': [],
                    'good_packets': [],
                    'prediction': [],
                    'previous_packets': []}

    # Filter string
    filter_input = None
    bad_icmp = []
    live_capture_counter = 1
    previous_live_capture_counter = 0
    
    # Local timezone
    local_tz = pytz.timezone('Singapore')

    # Default constructor
    def __init__(self, interface_name=None, machine_ip=None):
        # Instance variables
        self.filter_info = [Signature("0: ICMP 192.168.56.101:any <> 192.168.56.103:any {'msg': '*'}"),
                            Signature("2: TCP 192.168.56.101:8000 <> 192.168.56.103:![8080-9000] {'msg': 'yeah'}")]
        self._protocols = ['icmp', 'tcp', 'udp']
        self._model = None
        
        # try:
        #     self._db = open_conn()
            
        #     if self._db.is_connected():
        #         db_info = self._db.get_server_info()
        #         print(f'Connected to database.')
                
        #         cursor = self._db.cursor()
                
        #         # Set global connection timeout arguments
        #         global_connect_timeout = f'SET GLOBAL connect_timeout=180'
        #         global_wait_timeout = f'SET GLOBAL connect_timeout=180'
        #         global_interactive_timeout = f'SET GLOBAL connect_timeout=180'
                
        #         cursor.execute(global_connect_timeout)
        #         cursor.execute(global_wait_timeout)
        #         cursor.execute(global_interactive_timeout)
                
        #         self._db.commit()
                
        # except mysql.connector.Error as mysql_err:
        #     print(mysql_err)
        # finally:
        #     cursor.close()
            
        # print(self._db.cursor())
        self._sniff_timeout = 2
        
        # Create alert log directory
        os.makedirs(self.PATHS['alert_log'], exist_ok=True)
        local = datetime.now(self.local_tz)
        datetime_str = local.strftime('%Y-%m-%d_%H-%M-%S')
        
        # Open alert log for recording alerts
        self.log_file = open(fr"{self.PATHS['alert_log']}{datetime_str}.log", "wt")

        # Try to load classification model
        try:
            with open(self.PATHS['model_path'], 'rb') as model_file:
                self._model = pickle.load(model_file)
        except Exception as e:
            print(e)

        # Check network interface name is provided for object initialization
        # if interface_name is not None:
        self.__interface_name = interface_name
        # if interface_name is not None
        if machine_ip is None:
            raise Exception("Please provide your machine's IP")
        else:
            self.__machine_ip = machine_ip
        # self.init_thread()

    def init_thread(self):
        self.live_capture_thread = Thread(target=self.live_capture_ready, name='live_capture_thread', daemon=True)
        self.process_live_packet_thread = Thread(target=self.process_live_packet, name='process_live_packet_thread',
                                                 daemon=True)
        self.classify_live_packets_thread = Thread(target=self.classify_live_packets,
                                                   name='classify_live_packets_thread',
                                                   daemon=True)
        self.store_packets_to_db_thread = Thread(target=self.store_packets_to_db, name='store_packets_to_db_thread',
                                                 daemon=True)
        # self.store_events_to_db_thread = Thread(target=self.store_events_to_db, name='store_events_to_db_thread',
        #                                         daemon=True)
        self.store_alerts_to_db_thread = Thread(target=self.store_alerts_to_db, name='store_alerts_to_db_thread',
                                                daemon=True)

        # Start threads
        time.sleep(3)
        print('\nStarting threads')
        self.live_capture_thread.start()
        self.process_live_packet_thread.start()
        self.classify_live_packets_thread.start()
        self.store_packets_to_db_thread.start()
        # self.store_events_to_db_thread.start()
        self.store_alerts_to_db_thread.start()

        # Join threads
        # time.sleep(3)
        print('Joining threads')
        self.live_capture_thread.join()
        self.process_live_packet_thread.join()
        self.classify_live_packets_thread.join()
        self.store_packets_to_db_thread.join()
        # self.store_events_to_db_thread.join()
        self.store_alerts_to_db_thread.join()

        # Debugging
        # self.start_live_capture()
        # print('Stopping threads')
        # self.live_capture_thread.set()
        # self.process_live_packet_thread.join()
        # self.store_packets_to_db_thread.join()
        # self.store_events_to_db_thread.join()
        # self.store_alerts_to_db_thread.join()
        # self.start_live_capture()

    # Start live capture by setting self._status['live_capture_status'] to True
    def start_live_capture(self):
        '''
        Check whether the live_capture_status is set to false, then
        set it to true and return true for successful change
        If it is already true, then return true for successful change
        '''
        if not self._status['live_capture_status'] or self._status['live_capture_status']:
            # Set ._status['live_capture_status'] to true
            self._status['live_capture_status'] = True
        #     return True
        # return False

    # live_capture_ready will be automatically executed after executing
    # start_live_capture
    def live_capture_ready(self):
        # self.start_live_capture()
        os.makedirs(self.PATHS['initial_pcap_save_path'], exist_ok=True)
        
        
        time.sleep(4)
        # if not os.path.exists(self.PATHS['initial_pcap_save_path']):
        #     os.mkdir(self.PATHS['initial_pcap_save_path'])
        
        # while self._status['live_capture_status']:
        while True:
            # print('In capturing live')
            if self._status['live_capture_status']:
                if self.live_capture_counter == 31:
                    self.live_capture_counter = 1
                print('In capturing live')
                try:
                    '''
                    working
                    '''
                    file_str = f"{self.PATHS['initial_pcap_save_path']}{self.live_capture_counter}.pcap"
                    file = open(file_str, 'w+').close()

                    print(f'Capturing live packets for {self._sniff_timeout} seconds')
                    live_capture_process = Popen(f"tshark -F pcap -i {self.__interface_name} -w {file_str} -a duration:2 -f \"ip host {self.__machine_ip}\"", shell=True)
                    live_capture_process.wait()
                    # Debugging
                    # print(self.live_packets['packets'][0])
                    # print(len(self.live_packets['packets']))
                    print(f'Capturing live packets completed.', end='\n')
                except KeyboardInterrupt:
                    print('Keyboard Interrupt')
                except Exception as e:
                    print(e)
                finally:
                    pass
                
                self._status['live_capture_status'] = False
                time.sleep(0.5)
                self._status['live_processing_status'] = True
                # time.sleep(1)
                self.live_capture_counter += 1
                # time.sleep(0.8)
            
            time.sleep(1)

    # process_livepacket will be executed automatically after executing
    # live_capture_ready
    def process_live_packet(self):
        os.makedirs(self.PATHS['final_pcap_save_path'], exist_ok=True)
        # self.live_capture_thread.join()
        # self.process_live_packet_thread.join()
        # time.sleep(2)
        # while self._status['live_processing_status']:
        f_time = None
        while True:
            # print('In processing live', end='\n')
            if self._status['live_processing_status']:
                print('In processing live')
                # time.sleep(1)
                temp_counter = self.live_capture_counter - 1
                print(f'Current counter in processing: ', temp_counter)
                packets = rdpcap(f"{self.PATHS['initial_pcap_save_path']}{temp_counter}.pcap")
                # print(packets)
                
                if temp_counter == 1 and len(packets) > 0:
                    # first_pkt_time = packets[0].time
                    temp_time = datetime.fromtimestamp(int(packets[0].time))
                    # f_time = temp_time.strftime('%Y-%m-%d_%H:%M:%S')
                    # Datetime now
                    f_time = datetime.now(self.local_tz).strftime('%Y-%m-%d_%H-%M-%S')
                
                if temp_counter == 30:
                    f_time = datetime.now(self.local_tz).strftime('%Y-%m-%d_%H-%M-%S')
                    merge_process = os.system(f"mergecap -w {self.PATHS['final_pcap_save_path']}{f_time}.pcap {self.PATHS['initial_pcap_save_path']}*.pcap")
                    
                self._status['live_capture_status'] = True
                self.bad_icmp = []
                # self._status['live_capture_status'] = True
                
                '''processing = {'stream_idx eg. 1': {'packets': [],
                                                      'statistics': {'duration': 0,
                                                                     'protocol': None,
                                                                     'src_ip': None,
                                                                     'src_port': None,
                                                                     'src_bytes': 0,
                                                                     'dest_ip': None,
                                                                     'dest_port': None,
                                                                     'dest_bytes': 0}}}'''
                
                # processed = {}
                track_conn = dict()
                
                if len(packets) > 0:
                    try:
                        # print('In processing live', end='\n')
                        # tempArea = self.live_packets['packets']
                        err_count = 0
                        temp_area = self.get_packet_time_relative(packets)
                        processed = {}
                        streams = temp_area.sessions()
                        # print(len(streams))
                        print('Streams: ', end='')
                        # pp.pprint(streams)
                        
                        prev_src = {'protocol': None,
                                    'src_ip': None,
                                    'src_port': None,
                                    'dest_ip': None,
                                    'dest_port': None}
                        
                        stream_idx = 0
                        for stream, value in streams.items():
                            orientation = 'source'
                            same = False
                            
                            token = stream.split(' ')
                            token_src_ip = list()
                            token_dest_ip = list()
                            # token_src_ip = token[1].split(':')
                            # token_dest_ip = token[3].split(':')
                            if token[0] == 'ICMP':
                                token_src_ip.append(token[1])
                                token_dest_ip.append(token[3])
                            else:
                                sep = token[1].rfind(':')
                                token_src_ip.append(token[1][:sep])
                                token_src_ip.append(token[1][sep+1:])
                                
                                sep = token[3].rfind(':')
                                token_dest_ip.append(token[3][:sep])
                                token_dest_ip.append(token[3][sep+1:])
                            
                            if token[0] in ['TCP', 'UDP', 'ICMP']:
                                processed[stream_idx] = {'packets': [],
                                                        'statistics': {'duration': 0,
                                                                    'protocol': None,
                                                                    'src_ip': token_src_ip[0],
                                                                    'dest_ip': token_dest_ip[0],
                                                                    'src_bytes': 0,
                                                                    'dest_bytes': 0,
                                                                    'direction': None}}
                                
                                processed[stream_idx]['statistics']['protocol'] = token[0]
                                
                                check_dest = {}
                                lap = 0
                                
                                for info in value:
                                    try:
                                        return_value = self.filter_packets(info)
                                        err_count += 1
                                        
                                    except ValueError as ve:
                                        print('packet err')
                                    else:
                                        # TBC
                                        if lap == 0:
                                            check_dest = {'protocol': return_value['protocol'],
                                                         'src_ip': return_value['src_ip'],
                                                         'src_port': return_value['src_port'],
                                                         'dest_ip': return_value['dest_ip'],
                                                         'dest_port': return_value['dest_port']}
                                            
                                            if len(track_conn) > 0:
                                                for ki, vi in track_conn.items():
                                                    # print(vi)
                                                    if check_dest == vi:
                                                        del processed[stream_idx]
                                                        stream_idx = int(ki)
                                                        orientation = 'dest'
                                                        same = True
                                            lap += 1
                                        
                                        processed[stream_idx]['packets'].append(return_value)
                                        processed[stream_idx]['statistics']['duration'] += float(decimal.Decimal(return_value['frame_time']))

                                        if return_value['direction'] is not None:
                                            if return_value['direction'] == 'outbound' and processed[stream_idx]['statistics']['direction'] is None:
                                                processed[stream_idx]['statistics']['src_bytes'] += return_value['packet_len']
                                                processed[stream_idx]['statistics']['direction'] = 'outbound'
                                            elif return_value['direction'] == 'unknown':
                                                if orientation == 'source':
                                                    processed[stream_idx]['statistics']['src_bytes'] += return_value['packet_len']
                                                elif orientation == 'dest':
                                                    processed[stream_idx]['statistics']['dest_bytes'] += return_value['packet_len']
                                                    
                                                processed[stream_idx]['statistics']['direction'] = return_value['direction']
                                            elif return_value['direction'] == 'inbound' and processed[stream_idx]['statistics']['direction'] is None:
                                                processed[stream_idx]['statistics']['dest_bytes'] += return_value['packet_len']
                                                processed[stream_idx]['statistics']['direction'] = 'inbound'
                                        # pass
                            
                            
                            # # Debugging
                            prev_src = {'protocol': return_value['protocol'],
                                        'src_ip': return_value['dest_ip'],
                                        'src_port': return_value['dest_port'],
                                        'dest_ip': return_value['src_ip'],
                                        'dest_port': return_value['src_port']}

                            track_conn[stream_idx] = prev_src
                            stream_idx = len(track_conn)
                        
                        # Store processed into self.live_packets['previous_packets']
                        self.live_packets['previous_packets'].append(processed)
                        print(f'Processing live packets completed.', end='\n\n')
                        print('Length of prev: ', len(processed))
                    except Exception as e:
                        print('error_count: ', err_count)
                        print('prcoess live packet error')

                # Update status
                self._status['live_classification_status'] = True
                self._status['live_processing_status'] = False
                
                # self._status['store_packets_to_db_status'] = True
            time.sleep(0.5)

    # Classify packets
    def classify_live_packets(self):
        # Store formatted packets to list
        formatted_packets = []

        while True:
            if self._status['live_classification_status'] and len(self.live_packets['previous_packets']) > 0:
                print('In classifying live')
                try:
                    # Get the previous packets
                    temp_area = self.live_packets['previous_packets'].pop(0)
                    print(len(temp_area))
                    # pp.pprint(temp_area)
                    if len(temp_area) > 0:
                        formatted_packets = []
                        prepared_conn = []
                        # Check signatures against packets
                        temp_area = self.check_packet_signature(temp_area)
                        
                        # Iterate all the previous packets and add a classification
                        for item, val in temp_area.items():
                            prepared_conn.append([int(val['statistics']['duration']), val['statistics']['protocol'], 
                                                val['statistics']['src_bytes'], val['statistics']['dest_bytes']])
                            # for sub_item in val[item]:
                            #     # sub_item['class'] = None
                            #     # print(sub_item)
                            #     formatted_packets.append(sub_item)
                        
                        # Model classification as follows
                        preprocessed = pd.DataFrame(np.asarray(prepared_conn), columns=['duration', 'protocol_type', 'src_bytes', 'dst_bytes'])
                        print(preprocessed)
                        prediction_list = self._model.predict(preprocessed)
                        print('Predictions: ', prediction_list)
                    
                        # Add the classification into the packets
                        for idx in range(len(prediction_list)):
                            # Assign alert status to each stream or connection
                            if prediction_list[idx] > 0:
                                temp_area[idx]['alert'] = 1
                                # self.live_packets['alerts'].append(temp_area[idx]['statistics'])
                            else:
                                temp_area[idx]['alert'] = 0
                                
                            # for pkt in temp_area[idx]['packets']:
                            #     pkt['classification'] = self.ATTACKS[prediction_list[idx]]
                            temp_area[idx]['statistics']['classification'] = self.ATTACKS[prediction_list[idx]]
                        # print('Classification done.')
                        self.live_packets['prediction'].append(temp_area)
                        print('Classification done.')
                        
                        # For debugging purposes only
                        # if self.store_packets_to_db_thread.is_alive():
                        #     print('store_db is still alive.')
                except Exception as e:
                    print('damn')
                    print(e)
                    # pp.pprint(temp_area)
                    # raise KeyboardInterrupt
                # pp.pprint(temp_area)
                # self.live_packets['prediction'].append(temp_area)
                # print(len(self.live_packets['prediction']))
                # pp.pprint(self.live_packets['prediction'])
                # self._status['store_packets_to_db_status'] = True
                self._status['store_alerts_to_db_status'] = True
                # time.sleep(0.5)
                self._status['live_classification_status'] = False
                
                # if self.store_packets_to_db_thread.is_alive():
                #     print('store_db is still alive.')
            # else:
            #     self.store_packets_to_db_thread.start()
            #     self.store_packets_to_db_thread.join()
                # self._status['store_packets_to_db_status'] = True
            time.sleep(0.5)
            
    # Check packets against signatures
    def check_packet_signature(self, pkt_dict):
        health = []
        try:
            for idx, rule in enumerate(self.filter_info):
                for k, v in pkt_dict.items():
                    # print(v['packets'])
                    for p in v['packets']:
                        try:
                            pkt_sign = Signature(p)
                        
                            if pkt_sign.is_equal_to_rule(rule):
                                p['filter_id'] = rule.s_id
                                # print(f'\n{rule.__repr__()} \n{pkt_sign.__repr__()}')
                                health.append('Unhealthy!')
                                # pass
                            # else:
                            #     health.append('healthy!')
                        except ValueError as ve:
                            # print(ve)
                            # print('wrong')
                            pass
                        except Exception as e:
                            print(e)
                            print('Cannot get signature from packet.')
        except Exception as err:
            print('Error getting signature.')
        # finally:
        # pp.pprint(pkt_dict)
        print('Number of bad packets: ', len(health))
        return pkt_dict

    # Stop the live capture
    def stop_live_capture(self):
        '''
        Check whether the live_capture_status is set to true, then
        set it to false and return true for successful change
        If it is already false, then return true for successful change
        '''
        if self._status['live_capture_status']:
            self._status['live_capture_status'] = False
            return True
        return True

    # Start file capture
    def start_file_capture(self, filename):
        file_status = False
        alert_file = ''
        
        try:
            self._status['file_capture_status'] = True
            # Folder path for splitting, another folder will be created from the input file
            # Path.mkdir(Path('../' + self.PATHS['split_path']), exist_ok=True)
            Path.mkdir(Path(self.PATHS['split_path']), exist_ok=True)
            alert_file = self.file_capture_ready(filename)
        except Exception as err:
            print('ERROR')
            self._status['file_capture_status'] = False
        finally:
            self._status['file_capture_status'] = False
            
        return alert_file

    # Start 2nd part of file capture
    def file_capture_ready(self, filename):
        '''
        Split file and return status
        '''
        ret_file = ''
        
        self._status['file_processing_status'] = True
        
        ret_file = self.process_file_packet(str(filename))
        
        return ret_file


    def process_file_packet(self, filename):
        '''
        filename is the file uploaded 
        1. Pass in the filename to be processed.
        Process = ['filter', 'split', 'classify', 'return alert log filename']
        '''
        
        return_file = ''
        
        if self._status['file_processing_status']:
            # processing_file = Popen([f"python3", f"{self.PATHS['process_script_path']}process_script.py", f"{filename}"],
            #                         shell=False, stdout=PIPE, stderr=PIPE)
            
            # for myLine in iter(processing_file.stdout.readline, b''):
            #     # my_line = myLine.decode().rstrip('\n')
            #     print(myLine)
            # output, err = processing_file.communicate()
            # print(err)
            try:
                processing_file = Popen([Path.cwd() / f"venv/bin/python3", f"{self.PATHS['process_script_path']}process_script.py", f"{filename}"],
                                        shell=False, stdout=PIPE, stderr=PIPE)
                
                for myLine in iter(processing_file.stdout.readline, b''):
                    my_line = myLine.decode().rstrip('\n')
            except Exception as e:
                print(f'Processing file packets error !')
                
                raise KeyboardInterrupt
            else:
                return_file = my_line
            finally:
                self._status['file_processing_status'] = False
        
        return return_file
    
    
    # Get alerts from file capture log
    def load_alerts_from_log(self, filename, start, end):
        next_n_lines = None
        
        with open(filename, 'r') as f:
            next_n_lines = list(line.rstrip('\n.')
                                for line in islice(f, start, end))
        # print(next_n_lines)
            
        return next_n_lines
    
    def stop_file_capture(self):
        self._status['file_capture_status'] = False
        self._status['file_processing_status'] = False
    
    # Get time relative for each packet
    def get_packet_time_relative(self, packets):
        count = 0
        for pkt in packets:
            if count == 0:
                pkt.sent_time = '0.00'
            else:
                pkt.sent_time = str(pkt.time - packets[count-1].time)
                
            count += 1
            
        return packets
        
    # Filter packets functions
    def filter_packets(self, a_pkt):
        tempDict = {'interface': self.__interface_name,
                    'protocol': None,
                    'src_ip': None,
                    'src_port': None,
                    # 'src_bytes': 0,
                    'dest_ip': None,
                    'dest_port': None,
                    'packet_len': None,
                    'frame_time': None,
                    'arrival_datetime': None,
                    'direction': None,
                    'payload': None,
                    'filter_id': None,
                    'classification': None}

        if isinstance(a_pkt, Ether):
            if 'IP' in a_pkt:
                try:
                    tempDict['protocol'] = a_pkt[2].name
                    tempDict['src_ip'] = a_pkt[1].src
                    tempDict['src_port'] = a_pkt[1].sport
                    tempDict['dest_ip'] = a_pkt[1].dst
                    tempDict['dest_port'] = a_pkt[1].dport
                    tempDict['packet_len'] = len(a_pkt)
                    tempDict['frame_time'] = a_pkt.sent_time
                    
                    temp_time = datetime.fromtimestamp(int(a_pkt.time))
                    tempDict['arrival_datetime'] = temp_time.strftime('%Y-%m-%d %H:%M:%S')
                    
                    if self.__machine_ip in tempDict['src_ip']:
                        tempDict['direction'] = 'outbound'
                    elif self.__machine_ip in tempDict['dest_ip']:
                            tempDict['direction'] = 'inbound'
                    else:
                        tempDict['direction'] = 'unknown'
                        
                    tempDict['payload'] = str(bytes(a_pkt[tempDict['protocol']].payload).decode(encoding='utf-8', errors='ignore'))
                    # tempDict['payload'] = a_pkt.payload
                except AttributeError as ae:
                    if 'ICMP' in a_pkt:
                        tempDict['protocol'] = a_pkt[2].name
                        tempDict['src_ip'] = a_pkt[1].src
                        # tempDict['src_port'] = 'any' # Not needed
                        tempDict['dest_ip'] = a_pkt[1].dst
                        # tempDict['dest_port'] = a_pkt[1].dport # Not needed
                        tempDict['packet_len'] = len(a_pkt)
                        tempDict['frame_time'] = a_pkt.sent_time
                        
                        temp_time = datetime.fromtimestamp(int(a_pkt.time))
                        tempDict['arrival_datetime'] = temp_time.strftime('%Y-%m-%d %H:%M:%S')
                        
                        if self.__machine_ip in tempDict['src_ip']:
                            tempDict['direction'] = 'outbound'
                        elif self.__machine_ip in tempDict['dest_ip']:
                            tempDict['direction'] = 'inbound'
                        else:
                            tempDict['direction'] = 'unknown'
                            
                        # tempDict['payload'] = bytes(a_pkt.payload).decode(encoding='ascii', errors='ignore')
                        tempDict['payload'] = str(bytes(a_pkt[tempDict['protocol']].payload).decode(encoding='utf-8', errors='ignore'))
                    else:
                        raise ValueError()
                except IndexError:
                    raise ValueError()
                    # pass
            else:
                # raise ValueError()
                pass

        return tempDict

    # Load rules
    def load_rules_from_db(self):
        # if not self._db or (self._db is None):
        #     raise Exception('Database object not found.')
        
        # try:
        #     pass
        # except Exception as err:
        #     print(err)
        pass
    
    def attach_db(self, db=None):
        if db is not None:
            self._db = db
            print(f'In attach_db of class Sniffer: {self._db}')
            # a = mysql2.connection
            # print(self._db.cursor())
            return True
        return False

    def remove_db(self):
        if self._db is not None:
            self._db = None
            return True
        return False

    def get_db(self):
        '''
        If there is database object attached, return the object.
        Else return false
        '''
        if self._db is not None or self._db:
            return self._db
        return False

    def store_packets_to_db(self):
        # Create a database connection
        try:
            self._live_cap_db = open_conn()
        except mysql.connector.Error as mysql_err:
            print(mysql_err)
        
        if not self._live_cap_db or (self._live_cap_db is None):
            raise Exception('Database object not found.')
        
        while True:
            if self._status['store_packets_to_db_status']:
                #  and len(self.live_packets['prediction']) > 0
                # print('In store packets to db')
                # if len(self.live_packets['prediction']) > 0:
                if len(self.live_packets['store']) > 0:
                    print('In store packets to db')
                    # to_store = self.live_packets['prediction'].pop(0)
                    to_store = self.live_packets['store'].pop(0)
                    print(len(to_store))
                    try:
                        insert_query = ('insert into packets '
                                        '(filter_id, interface, frame_time, src_ip, src_port, dest_ip, dest_port, protocol, ' + \
                                        'packet_length, classification, arrival_datetime, direction, alert_id) '
                                        'values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)')
                        add_values_list = []
                        # add_values = (pktDict['interface'], pktDict['frame_time'], pktDict['src_ip'], pktDict['src_port'],
                        #               pktDict['dest_ip'], pktDict['dest_port'], pktDict['protocol'], pktDict['packet_len'],
                        #               pktDict['arrival_datetime'])
                        # for item in self.live_packets['prediction']:
                        #     add_tuple = (item['interface'], item['frame_time'], item['src_ip'], item['src_port'],
                        #                  item['dest_ip'], item['dest_port'], item['protocol'], item['packet_len'],
                        #                  item['arrival_datetime'])
                        #     add_values_list.append(add_tuple)
                        
                        for k, v in to_store.items():
                            for p in v['packets']:
                                add_tuple = (p['filter_id'], p['interface'], p['frame_time'], p['src_ip'], p['src_port'],
                                             p['dest_ip'], p['dest_port'], p['protocol'], p['packet_len'],
                                             v['statistics']['classification'], p['arrival_datetime'], p['direction'], 
                                             v['statistics']['alert_id'])
                                add_values_list.append(add_tuple)

                        print('Length: ', len(add_values_list))
                        # pp.pprint(add_values_list[-1])
                        a_db = self._live_cap_db
                        a_cursor = a_db.cursor(prepared=True)
                        a_cursor.executemany(insert_query, add_values_list)
                        # rowid = a_cursor.lastrowid
                        # affected_rows = a_cursor.rowcount
                        
                        # if affected_rows > 0:
                        #     # return rowid
                        #     print(rowid)
                        # print(f'Affected rows: {affected_rows}')
                        
                        a_db.commit()
                        rowid = a_cursor.lastrowid
                        affected_rows = a_cursor.rowcount
                        # print(f'Affected rows: {a_cursor.rowcount}')
                        # a_cursor.close()
                        # print(f'Affected rows: {a_cursor.rowcount}')
                        # self._status['store_packets_to_db_status'] = False

                        if affected_rows != -1:
                            print(f'Last insert id: {rowid}')
                            print(f'Affected rows: {abs(affected_rows)}')
                            # return rowid
                        else:
                            print('Insertion error!')
                        # self._status['store_packets_to_db_status'] = False
                    except Exception as e:
                        self._status['store_packets_to_db_status'] = False
                        # ALTER TABLE packets AUTO_INCREMENT = rowid
                        print(e)
                    finally:
                        a_cursor.close()
                        self._status['store_packets_to_db_status'] = False
                        # pass
                    # self._status['store_packets_to_db_status'] = False
            db_info = self._live_cap_db.get_server_info()
        # pass

    def store_events_to_db(self):
        pass

    def store_alerts_to_db(self):
        # Create a database connection
        try:
            self._live_alert_db = open_conn()
        except mysql.connector.Error as mysql_err:
            print(mysql_err)
            
        if not self._live_alert_db or (self._live_alert_db is None):
            raise Exception('Database object not found.')
        
        while True:
            if self._status['store_alerts_to_db_status']:
                if len(self.live_packets['prediction']) > 0:
                    print('In storing alerts to db.')
                    
                    check_alerts = self.live_packets['prediction'].pop(0)
                    print('Length of check alerts: ', len(check_alerts))
                    
                    try:
                        alert_query = ('insert into alerts '
                                       '(protocol, src_ip, dest_ip, alert_datetime, classification, direction) '
                                       'values (%s, %s, %s, %s, %s, %s)')
                        
                        add_values = ()
                        
                        # Datetime now
                        now = datetime.now(self.local_tz).strftime('%Y-%m-%d %H:%M:%S')
                        
                        a_db = self._live_alert_db
                        a_cursor = a_db.cursor(prepared=True)
                        
                        for k, v in check_alerts.items():
                            if v['alert'] == 1:
                                add_values = (v['statistics']['protocol'], v['statistics']['src_ip'], 
                                             v['statistics']['dest_ip'], now, v['statistics']['classification'], 
                                             v['statistics']['direction'])
                                
                                
                                a_cursor.execute(alert_query, add_values)
                                a_db.commit()
                                rowid = a_cursor.lastrowid
                                affected_rows = a_cursor.rowcount
                                
                                if affected_rows != -1:
                                    v['statistics']['alert_id'] = rowid
                                    v['statistics']['alert_datetime'] = now
                                    self.live_packets['alerts'].append(v['statistics'])
                                    print('Alerts: ')
                                    pp.pprint(v['statistics'])
                                    print()
                                    print(f'Last insert id: {rowid}')
                                    print(f'Affected rows: {abs(affected_rows)}')
                                    # pickle.dump(v['statistics'], self.log_file)
                                    self.log_file.write(f"{str(v['statistics'])}\n")
                                    self.log_file.flush()
                                    # return rowid
                                else:
                                    print('Add alerts to db error!')
                            else:
                                v['statistics']['alert_id'] = None
                    except Exception as e:
                        self._status['store_alerts_to_db_status'] = False
                        print(e)
                    finally:
                        a_cursor.close()
                        print(f"Length of alerts: {len(self.live_packets['alerts'])}")
                        self.live_packets['store'].append(check_alerts)
                        # print()
                        # pp.pprint(check_alerts)
                        self._status['store_packets_to_db_status'] = True
                        self._status['store_alerts_to_db_status'] = False
            db_info = self._live_alert_db.get_server_info()
            
        # pass

    def store_filter_to_db(self):
        pass

    def update_filter_to_db(self):
        pass

    def delete_filter_to_db(self):
        pass
    
    # Get live alerts for the dashboard
    def get_available_live_alerts(self):
        return_list = []
        
        if len(self.live_packets['alerts']) > 0:
            # while self.live_packets['alerts']:
            # return return_list.append(self.live_packets['alerts'].pop(0))
            return self.live_packets['alerts'].pop(0)
            
            # return return_list
        
        return {}
    
    # Get total number of potential threats 
    def get_total_potential_threats(self):
        db = None
        return_res = list()
        
        try:
            db = open_conn()
            cursor = db.cursor(prepared=True)
        except mysql.connector.Error as mysql_err:
            print(mysql_err)
        except Exception as e:
            raise Exception('No database connection.')
        else:
            # Get total number of potential threats
            try:
                # Prepare query statement
                query_stat = ('select classification, count(*) as total from alerts '
                              'group by classification '
                              'order by count(*) desc')
            
                # Get query result
                result = cursor.execute(query_stat)
                result = cursor.fetchall()
            except mysql.connector.Error as mysql_err2:
                print(mysql_err2)
            except Exception as e:
                raise Exception('Cannot do query.')
            else:
                return_res = [dict(zip(cursor.column_names, tup)) for tup in result]
        finally:
            if cursor is not None:
                cursor.close()
            
            if db is not None:
                db.close()
        
        return return_res
    
    # Get total threats previous month
    def get_threats_prev_mth(self, **kwargs):
        db = None
        return_res = list()
        
        try:
            db = open_conn()
            cursor = db.cursor(prepared=True)
        except mysql.connector.Error as mysql_err:
            print(mysql_err)
        except Exception as e:
            raise Exception('No database connection.')
        else:
            # Get total number of threats for previous month
            try:
                # Prepare query statement
                query_stat = ('select classification, count(*) as total from alerts '
                              'where alert_datetime between date_sub(%s, interval 1 month) and %s '
                              'group by classification')
                
                # Prepare date for previous month
                prev_month_date = datetime.strptime(kwargs['req_datetime'], "%Y-%m-%d %H:%M:%S")
                prev_month_date = prev_month_date.replace(month=prev_month_date.month-1)
                
                # Get last day of previous month
                last_day = calendar.monthrange(prev_month_date.year, prev_month_date.month)[1]
                
                # Prepare first date of previous month
                first_date = prev_month_date.replace(day=1).strftime('%Y-%m-%d')
                
                # Prepare last date of previous month
                last_date = prev_month_date.replace(day=last_day).strftime('%Y-%m-%d')
                
                # Prepare values for query
                query_val = (first_date, last_date)
                
                result = cursor.execute(query_stat, query_val)
                result = cursor.fetchall()
                
                return_res = [dict(zip(cursor.column_names, tup)) for tup in result]
            except Exception as e:
                print(f'Unable to prepare date!')
            else:
                return_res = [dict(zip(cursor.column_names, tup)) for tup in result]
        finally:
            if cursor is not None:
                cursor.close()

            if db is not None:
                db.close()
        
        return return_res
    
    # Get alerts_dist
    def get_alerts_dist(self):
        db = None
        return_res = list()

        try:
            db = open_conn()
            cursor = db.cursor(dictionary=True)
        except mysql.connector.Error as mysql_err:
            print(mysql_err)
        except Exception as e:
            raise Exception('No database connection.')
        else:
            # Get total number of threats for previous month
            try:
                # Prepare query statement
                query_stat = ('select classification, count(*) as total from alerts '
                              'group by classification '
                              'order by classification asc')

                result = cursor.execute(query_stat)
                result = cursor.fetchall()

            except Exception as e:
                print(f'Unable to prepare date!')
            else:
                return_res = result
        finally:
            if cursor is not None:
                cursor.close()

            if db is not None:
                db.close()

        return return_res
    
    # Get alerts protocol dist
    def get_alerts_protocol_dist(self):
        db = None
        return_res = list()

        try:
            db = open_conn()
            cursor = db.cursor(dictionary=True)
        except mysql.connector.Error as mysql_err:
            print(mysql_err)
        except Exception as e:
            raise Exception('No database connection.')
        else:
            # Get total number of threats for previous month
            try:
                # Prepare query statement
                query_stat = ('select protocol, count(*) as total from alerts '
                              'group by protocol '
                              'order by protocol asc')

                result = cursor.execute(query_stat)
                result = cursor.fetchall()

            except Exception as e:
                print(f'Unable to prepare date!')
            else:
                return_res = result
        finally:
            if cursor is not None:
                cursor.close()

            if db is not None:
                db.close()

        return return_res
    
    
    # Destructor
    def __del__(self):
        self._live_alert_db.close()
        self._live_cap_db
        self.log_file.close()


# Debugging purposes only
# print('okay')
