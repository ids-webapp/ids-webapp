# System Libraries
import re
import copy
import decimal

# Third-party libraries
from scapy.all import *


class Signature:
    # regex constant
    REGEXP = re.compile(r'''
    # Signature ID or Rules ID (s_id)
    # Eg. 100: or blank
    ^(\d{,99999}:\s)?
    
    # Protocol Type (protocol)]
    # Eg. TCP or UDP or ICMP
    ([A-Z]{,4}\s)
    
    # Source IP Address
    # Eg. !192.168.0.1 or 192.168.1.1 or any
    ((!?\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:)|(any:))
    
    # Source Port Number
    # Eg. !4000 or 4000 or ![4000-4100] or [4000-4100]
    ((!?(\d{1,6}\s|\[\d{1,6}-\d{1,6}\]\s))|any\s)
    
    # Direction of flow
    # Eg. bidirectional '<>' or unidirectional '->'
    ((<>|->)\s)
    
    # Destination IP Address
    # Eg. !192.168.0.1 or 192.168.1.1 or any
    ((!?\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:)|(any:))
    
    # Destination Port Number
    # Eg. !4000 or 4000 or ![4000-4100] or [4000-4100]
    ((!?(\d{1,6}\s|\[\d{1,6}-\d{1,6}\]\s))|any\s)
    
    # Payload
    # Eg. ('msg': '*') or ('msg': 'attack')
    (\{\'msg\':\s)(\'[\w|\W]*\'\})$
    ''', re.VERBOSE)

    # Default constructor
    def __init__(self, unknown_obj=None):
        super(Signature, self).__init__()
        if unknown_obj is not None:
            # print('Creating obj now')
            self.__activate_obj(unknown_obj)

    # Activate signature object if an object is provided
    def __activate_obj(self, unresolved_obj):
        # Check whether unresolved_obj is a packet
        if isinstance(unresolved_obj, dict):
            # print(f'In create obj')
            # print(unresolved_obj)
            direction = '->'
            s_id = None
            # if unresolved_obj['protocol'] in ['TCP', 'UDP']:
            try:
                protocol = unresolved_obj['protocol']
                src_ip = unresolved_obj['src_ip']
                src_port = unresolved_obj['src_port']
                dest_ip = unresolved_obj['dest_ip']
                dest_port = unresolved_obj['dest_port']
                # payload = str(bytes(unresolved_obj['payload']).decode(encoding='ascii', errors='ignore'))
                payload = str(unresolved_obj['payload'])
                # payload = '*'
            except AttributeError:
                if 'ICMP' in unresolved_obj:
                    try:
                        protocol = unresolved_obj['protocol']
                        src_ip = unresolved_obj['src_ip']
                        src_port = None
                        dest_ip = unresolved_obj['dest_ip']
                        dest_port = None
                        # payload = bytes(unresolved_obj['payload']).decode(encoding='ascii', errors='ignore')
                        payload = str(unresolved_obj['payload'])
                    except ValueError:
                        raise ValueError('ValueError Exception in parsing ICMP packet')
                else:
                    raise ValueError('Error parsing packet')
            except IndexError:
                raise ValueError()
            # else:
            #     raise ValueError()
        # Check whether unresolved_obj is a string which means rules
        elif isinstance(unresolved_obj, str):
            # rule_str = unresolved_obj.split(' ')
            # Split the rule by parentheses to get payload
            rule_str = re.split(r"\s*(\{.*?\})$", unresolved_obj)
            rule_str = list(filter(None, rule_str))
            # Split the string at index 0 by whitespace to obtain the rest of information
            rule_str2 = rule_str[0].split(' ')
            rule_str2.append(rule_str[1])

            # 6 elements means the string is complete
            if len(rule_str2) == 6:
                # Extract Ip address and port for source and destination
                src_ip_port = rule_str2[2].split(':')
                dest_ip_port = rule_str2[4].split(':')

                s_id = rule_str2[0].strip(':')
                protocol = rule_str2[1]
                src_ip = src_ip_port[0]
                src_port = src_ip_port[1]
                direction = rule_str2[3]
                dest_ip = dest_ip_port[0]
                dest_port = dest_ip_port[1]

                payload_str = rule_str2[5]
                # payload_dict_str = "{" + payload_str.strip('()') + "}"
                payload_dict = eval(payload_str)
                payload = payload_dict['msg']
            else:
                raise ValueError('Error parsing rule.')
        else:
            raise ValueError(unresolved_obj, 'cannot be initialized.')

        del unresolved_obj
        self.s_id = s_id
        self.protocol = protocol
        self.src_ip = src_ip
        self.src_port = src_port
        self.direction = direction
        self.dest_ip = dest_ip
        self.dest_port = dest_port
        self.payload = payload

    # Check the format of rule
    @classmethod
    def check_rule_format(cls, rule):
        if cls.REGEXP.match(rule):
            return True
        return False

    # Check whether both objects are equal
    def is_equal_to_rule(self, rule_sign):
        '''
        packet_sign = packet's signature
        rule_sign = rule
        self which is also packet's signature, is always without !/any/<>/portRange
        and is a packet signature
        '''
        if isinstance(rule_sign, self.__class__):
        # if rule_sign:
            # Check flow direction
            if rule_sign.direction == '<>':
                src_dest, dest_src = self.get_both_directions(rule_sign)
                return self.is_equal_to_rule(src_dest) or self.is_equal_to_rule(dest_src)

            # Check protocol type
            if rule_sign.protocol != 'any':
                if not self.check_fields_equality(rule_sign.protocol, 'protocol'):
                    return False

            # Source IP
            if rule_sign.src_ip != 'any':
                if not (self.check_fields_equality(rule_sign.src_ip, 'src_ip') == True):
                    return False
                
            # Source Port
            if not rule_sign.src_port != 'any' and self.src_port is not None:
                if not self.check_fields_equality(rule_sign.src_port, 'src_port'):
                    return False
                
            # Destination IP
            if rule_sign.dest_ip != 'any':
                if not self.check_fields_equality(rule_sign.dest_ip, 'dest_ip'):
                    return False

            # Destination Port
            if rule_sign.dest_port != 'any' and self.src_port is not None:
                if not self.check_fields_equality(rule_sign.dest_port, 'dest_port'):
                    return False

            if rule_sign.payload != '*':
                if not self.check_fields_equality(rule_sign.payload, 'payload'):
                    return False

            return True
        else:
            return False

    def get_both_directions(self, signature):
        # Source to Destination
        src = copy.deepcopy(signature)
        src.direction = '->'

        # Destination to Source
        dest = copy.deepcopy(signature)
        dest.direction = '->'
        dest.src_ip = src.dest_ip
        dest.src_port = src.dest_port
        dest.dest_ip = src.src_ip
        dest.dest_ip = src.src_port

        return src, dest

    def check_fields_equality(self, value, field_name):
        # Checking protocol and ip addresses
        if field_name in ['protocol', 'src_ip', 'dest_ip']:
            if value in ['ICMP', 'UDP', 'TCP'] and self.protocol in ['ICMP', 'UDP', 'TCP'] and field_name == 'protocol':
                if value == self.protocol:
                    return True
                # else:
                #     return False
            else:
                if field_name == 'src_ip':
                    if value[0] == '!' and self.src_ip == value[1:]:
                        return False
                    elif not value[0] == '!' and not self.src_ip == value:
                        return False
                elif field_name == 'dest_ip':
                    if value[0] == '!' and self.dest_ip == value[1:]:
                        return False
                    elif not value[0] == '!' and not self.dest_ip == value:
                        return False
                return True
            return False
        # Check payload
        elif field_name == 'payload':
            if value == '*':
                return True
            else:
                if value in self.payload:
                    return True
                else:
                    return False
        # Check ports
        else:
            try:
                if '-' in value:
                    # if value[0] == '!':
                    # Check whether there is port range specified
                    if '!' in value:
                        # port_split = value[1:].split(':')
                        # start = port_split[0][1:]
                        # end = port_split[1][:-1]
                        start, end = map(int, value[2:-1].split('-'))
                    else:
                        # port_split = value.split(':')
                        # start = port_split[0][1:]
                        # end = port_split[1][:-1]
                        start, end = map(int, value[1:-1].split('-'))
                else:
                    if value[0] == '!':
                        start = int(value[1:])
                        end = int(value[1:])
                    else:
                        start = int(value)
                        end = int(value)

                port_range = list(range(start, end + 1))

                if field_name == 'src_port':
                    check_port = int(self.src_port)
                else:
                    check_port = int(self.dest_port)
            except ValueError:
                print(f'Port number might be any')
                return True
            else:
                if '!' in value and check_port in port_range:
                    return False
                elif '!' in value and check_port not in port_range:
                    return True
                elif '!' not in value and check_port in port_range:
                    return True

    # To be confirmed
    def load_rules(self):
        pass

    # String representation of Signature object
    def __str__(self):
        return f"{self.protocol} {self.src_ip}:{self.src_port} {self.direction} " + \
                f"{self.dest_ip}:{self.dest_port} {self.payload}"

    # Unambiguous string representation of Signature object
    def __repr__(self):
        return f"Rule {self.s_id}: {self.__str__()}"
