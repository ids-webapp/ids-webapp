import mysql.connector

conn_args = {
    'user': 'root',
    'password': '',
    'host': 'localhost',
    'database': 'csit321'
}

db = None

def open_conn():
    global db
    
    try:
        db = mysql.connector.connect(**conn_args, connection_timeout=180)
        
        # print(db, end='\n')
        # print()
        return db
    except Exception as e:
        print(e)
        try:
            db.close()
        except Exception as f:
            pass

# open_conn()