import threading
from sys import argv
from pathlib import Path
from subprocess import Popen, PIPE, call
import queue
import sys
import os
import time, pprint
import pytz
from dateutil.relativedelta import relativedelta
from datetime import timedelta, datetime, time
from concurrent.futures import ThreadPoolExecutor, as_completed, ProcessPoolExecutor
import multiprocessing
from functools import partial
from scapy.all import *
import humanize
from scapy.layers.l2 import Ether

from ext_fns import get_packet_time_relative, filter_packets, load_model, check_packet_signature, classify_live_packets
from ext_fns import ATTACKS
from db_conn import open_conn


def collect_stats_data(filename, lock, a_count, output_file):
    # filename_token = filename.split('_')
    # packets = PcapReader(filename)
    # global counter
    processed = {}
    stream_idx = 0
    classified = None

    try:
        pkt_reader = rdpcap(str(filename))
        # pkt_list = [pkt for pkt in pkt_reader]
    except Exception as err:
        print(err)

    with lock:
        # print(str(filename))

        with open(str(output_file), 'a+') as open_file:
            # open_file.write(f'{str()}')
        # a_count['counter'] += 1
            # print(f"{a_count['counter']}")
            try:
                pkt_reader = get_packet_time_relative(pkt_reader)
                sessions = pkt_reader.sessions()
                # pprint.pprint(f'Streams: {sessions}')
            except Exception as err2:
                print(err2)
            else:
                try:
                    # orientation = 'source'
                    track_conn = dict()
                    # same = False

                    if len(pkt_reader) > 0:
                        prev_src = {'protocol': None,
                                    'src_ip': None,
                                    'src_port': None,
                                    'dest_ip': None,
                                    'dest_port': None}

                        for stream, value in sessions.items():
                            orientation = 'source'
                            same = False

                            token = stream.split(' ')
                            token_src_ip = list()
                            token_dest_ip = list()

                            if token[0] == 'ICMP':
                                token_src_ip.append(token[1])
                                token_dest_ip.append(token[3])
                            else:
                                sep = token[1].rfind(':')
                                token_src_ip.append(token[1][:sep])
                                token_src_ip.append(token[1][sep + 1:])

                                sep = token[3].rfind(':')
                                token_dest_ip.append(token[3][:sep])
                                token_dest_ip.append(token[3][sep + 1:])

                            if token[0] in ['TCP', 'UDP', 'ICMP']:
                                processed[stream_idx] = {'packets': [],
                                                         'statistics': {'duration': 0,
                                                                        'protocol': None,
                                                                        'src_ip': token_src_ip[0],
                                                                        'dest_ip': token_dest_ip[0],
                                                                        'src_bytes': 0,
                                                                        'dest_bytes': 0,
                                                                        'direction': None}}

                                processed[stream_idx]['statistics']['protocol'] = token[0]

                                check_dest = {}
                                lap = 0

                                for info in value:
                                    try:
                                        return_value = filter_packets(info)
                                        # err_count += 1

                                    except ValueError as ve:
                                        print('packet err')
                                    else:
                                        # TBC
                                        if lap == 0:
                                            check_dest = {'protocol': return_value['protocol'],
                                                          'src_ip': return_value['src_ip'],
                                                          'src_port': return_value['src_port'],
                                                          'dest_ip': return_value['dest_ip'],
                                                          'dest_port': return_value['dest_port']}

                                            # print(f'prev_src: {prev_src}')
                                            # print(f'check_dest: {check_dest}')
                                            # if len(track_conn) == 0:
                                                # if all(prev_src.values()):
                                                # if not (None in prev_src.values()):
                                                #     track_conn[stream_idx] = prev_src
                                                # track_conn[stream_idx] = check_dest

                                            if len(track_conn) > 0:
                                                for ki, vi in track_conn.items():
                                                    # print(vi)-
                                                    if check_dest == vi:
                                                        del processed[stream_idx]
                                                        stream_idx = int(ki)
                                                        orientation = 'dest'
                                                        same = True
                                                        # print(f'check_dest: {check_dest}')
                                                        # print(f'Same!')

                                                # if not same:
                                                #     track_conn[stream_idx] = prev_src
                                            # print(f'prev_src: {prev_src}')
                                            # print(f'check_dest: {check_dest}')
                                            # if check_dest == prev_src:
                                            #     # processed.popitem()
                                            #     # Delete dictionary created earlier as both details are the same
                                            #     del processed[stream_idx]
                                            #     stream_idx -= 1
                                            #     dir = 'dest'
                                            lap += 1

                                        # print(track_conn)
                                        processed[stream_idx]['packets'].append(return_value)
                                        processed[stream_idx]['statistics']['duration'] += float(
                                            decimal.Decimal(return_value['frame_time']))

                                        if return_value['direction'] is not None:
                                            if return_value['direction'] == 'outbound' and processed[stream_idx]['statistics']['direction'] is None:
                                                processed[stream_idx]['statistics']['src_bytes'] += return_value['packet_len']
                                                processed[stream_idx]['statistics']['direction'] = 'outbound'
                                            elif return_value['direction'] == 'unknown':
                                                if orientation == 'source':
                                                    processed[stream_idx]['statistics']['src_bytes'] += return_value['packet_len']
                                                elif orientation == 'dest':
                                                    processed[stream_idx]['statistics']['dest_bytes'] += return_value['packet_len']
                                                    
                                                processed[stream_idx]['statistics']['direction'] = return_value['direction']
                                            elif return_value['direction'] == 'inbound' and processed[stream_idx]['statistics']['direction'] is None:
                                                processed[stream_idx]['statistics']['dest_bytes'] += return_value['packet_len']
                                                processed[stream_idx]['statistics']['direction'] = 'inbound'
                                        # pass
                            # stream_idx += 1

                            # # Debugging
                            prev_src = {'protocol': return_value['protocol'],
                                        'src_ip': return_value['dest_ip'],
                                        'src_port': return_value['dest_port'],
                                        'dest_ip': return_value['src_ip'],
                                        'dest_port': return_value['src_port']}

                            # if len(track_conn) == 0:
                            track_conn[stream_idx] = prev_src
                            stream_idx = len(track_conn)
                            # stream_idx += 1
                except Exception as err3:
                    # print(err3)
                    print(f'Cannot get full data!')
                else:
                    try:
                    # open_file.write(f'{str(processed)}#{filename.name}\n')
                        classified = classify_live_packets(processed)
                    except Exception as err4:
                        print(f'Cannot classify packets!')
            finally:
                a_count['counter'] += 1
                print('Collecting file stats class completed. \n')

    # return a_count
    return {'counter': a_count['counter']-1,
            'data': classified}

def ret_fn(val):
    return val


if __name__ == "__main__":
    '''
        Step 1: Get input file
        Step 2: Create filter directory
        Step 3: Filter file
        Step 4: Create split directory
        Step 5: Split input files into parts
        Step 6: Process the file parts using multiprocessing
        Step 5: Collect alerts
        Step 6: Display statistics
        Step 7: Display alert filename
    '''
    # path = Path('D:/pcap_files/2021-10-01/')
    # path = Path('D:/pcap_files/2021-09-30-01/')

    # Step 1: Get input file
    # input_file = Path.home() / f"Desktop/merge_2021-10-01/merge10mb.pcap"
    # input_file = Path.home() / f"Desktop/merge_2021-10-01/wannacry.pcap"
    input_file = Path.cwd() /Path(argv[1])
    print(str(input_file))
    # input_file = Path(filename)
    
    # Step 2: 
    # Create filter directory
    filter_path = Path.cwd() / Path('fyp_pythonflask/filtered/')
    Path.mkdir(filter_path, exist_ok=True)
    
    # Step 3:
    # Filter the capture file
    filter_file = filter_path / f'{input_file.stem}2.pcap'
    # filter_file = Path(f'fyp_pythonflask/{input_file.stem}2.pcap')
    filter_process = Popen(["tshark", f"-r", f"{str(input_file.resolve())}", f"-w", f"{str(filter_file.resolve())}",
                           f"-Y", f"ip and (tcp or udp or icmp)"], stdout=PIPE, stderr=PIPE).wait()
    # filter_process = Popen(["tshark", f"-r", f"{str(input_file)}", f"-w", f"/home/csit115/Desktop/ids-webapp/fyp_pythonflask/filtered/merge1mb2.pcap",
    #                        f"-Y", f"ip and (tcp or udp or icmp)"], stdout=PIPE, stderr=PIPE).wait()

    # Step 4:
    # Folder path for splitting, another folder will be created from the input file
    Path.mkdir( Path.cwd() / Path('fyp_pythonflask/split/'), exist_ok=True)
    # split_path =  Path.cwd() / Path('fyp_pythonflask/split') / f'{filter_file.stem}/'
    split_path =  Path.cwd() / Path('fyp_pythonflask/split/') / f'{filter_file.stem}/'

    # Step 4: Create split directory
    # os.makedirs(Path('D:/pcap_files/split/') / f"{input_file.stem}/", exist_ok=True)
    Path.mkdir(split_path, exist_ok=True)

    # Step 5: Split input file into parts, continued later
    # split_process = Popen(f"editcap -i 2 \"{str(input_file)}\" \"{str(split_path)}/.pcap\"", shell=False).wait()
    split_process = Popen(["editcap", "-i", f"2", f"{str(filter_file.resolve())}",
                          f"{str(split_path.resolve())}/.pcap"], stdout=PIPE, stderr=PIPE).wait()
    # split_process = Popen(["editcap", "-i", f"2", f"/home/csit115/Desktop/ids-webapp/fyp_pythonflask/filtered/merge1mb2.pcap",
    #                       f"/home/csit115/Desktop/ids-webapp/fyp_pythonflask/split/merge1mb2/.pcap"], stdout=PIPE, stderr=PIPE).wait()

    # Step 6: Process the parts, continued later
    # Folder path for processing the file parts
    process_path = split_path

    # Step 7:
    # Log file output
    output_file =  Path.cwd() / Path('fyp_pythonflask/file_alert_logs/')
    Path.mkdir(output_file, exist_ok=True)

    local = datetime.now(pytz.timezone('Singapore'))
    datetime_str = local.strftime('%Y-%m-%d_%H-%M-%S')
    output_file = output_file / f'{datetime_str}-MP-alert.log'

    # sum(f.stat().st_size for f in Path('B:/decision-tree/tcp-udp-icmp-done2/').rglob('*') if f.is_file())

    m = multiprocessing.Manager()
    lock = m.Lock()
    counter = 1
    job_count = 0
    MAX_JOBS_IN_QUEUE = 100
    file_capture = True
    my_list = []
    my_list.append(0)

    counter_dict = m.dict({'counter': 0,
                            'first_size': 0,
                            'second_size': 0,
                            'total_size': 0})

    total_class = {'normal': 0,
                    'abnormal': 0,
                    'total': 0}

    # files_left = len(os.listdir(process_path))
    files_left = len([p for p in process_path.iterdir()])
    # print(files_left)

    # Dictionary to hold all the jobs submitted
    jobs = dict()

    # Statuses
    status = {'success': 0,
                'failed': 0}
    '''
    1. Get filename
    2. Pass in filename
    3. Read in pcap using PcapReader
    4. Get the time of the first packet
    5. Format it and rename the file
    '''
    # file_list = iter(os.listdir(process_path))
    # file_list = iter(process_path.iterdir())
    file_list = process_path.iterdir()
    # file_list = range(5)

    cycle_count = 0
    alert_id = 1
    return_file = ''

    try:
        start_time = time.time()
        # Get total number of files in the directory
        # print(len(os.listdir(path)))

        # Print all filenames in the directory
        # print(os.listdir(path))
        with ProcessPoolExecutor(max_workers=2) as executor:
            while files_left:
                # if cycle_count == 50:
                #     file_capture = False
                
                for file in file_list:
                    # executor.submit(collect_stats, lock, counter_dict)
                    # future = executor.submit(collect_stats_data, (process_path / file), lock, counter_dict, output_file)
                    future = executor.submit(collect_stats_data, file, lock, counter_dict, output_file)
                    jobs[future] = job_count
                    job_count += 1

                    # Limit the job submission for now job
                    if len(jobs) > MAX_JOBS_IN_QUEUE:
                        break
                    
                if not file_capture:
                    executor.shutdown(wait=True)

                    for jobf in jobs:
                        if not jobf.done():
                            jobf.cancel()

                    break

                with open(str(output_file), 'a+') as open_file:
                    for f in as_completed(jobs):
                        # output_count += 1
                        files_left -= 1  # one down - many to go...
                        res = f.result()
                        # res = eval(str(res))
                        # print(res['data'][0]['statistics'])
                        # print(res['data'][0])
                        for k, v in res['data'].items():
                            # List to hold value_stats
                            add_values_list = []
                            # Write to file for logging and analysis
                            # if v['statistics']['classification'] is None:
                            #     alert_id = 0
                            # else:
                            #     alert_id = v['statistics']['classification']
                                
                            # open_file.write(f"{res['counter']}#{str(v['statistics']['classification'])}#{str(v['packets'])}\n")

                            if v['statistics']['classification'] in ['DoS', 'Probe', 'U2R', 'R2L']:
                                total_class['abnormal'] += 1
                                
                                open_file.write(f"{res['counter']}#{alert_id}#{v['statistics']}\n")
                                
                                alert_id += 1
                            elif v['statistics']['classification'] == 'Normal':
                                total_class['normal'] += 1

                            # print(res['data'][0]['statistics']['classification'])

                            # # For loop to create value statement and execute query
                            # for p in v['packets']:
                            #     value_stat = (p['filter_id'], p['interface'], p['frame_time'], p['src_ip'], p['src_port'],
                            #                   p['dest_ip'], p['dest_port'], p['protocol'], p['packet_len'],
                            #                   v['statistics']['classification'], p['arrival_datetime'], p['direction'])

                            #     add_values_list.append(value_stat)
                                
                            # cursor.executemany(pkt_stat, add_values_list)
                            # db_obj.commit()
                        
                        if res:
                            status['success'] += 1
                        elif not res:
                            status['failed'] += 1
                        # counter += 1
                        # print(f'\nCompleted. --{f}')
                        del jobs[f]

                        break  # give a chance to add more jobs
                cycle_count += 1
    except Exception as err:
        print(err)
    else:
        return_file = str(output_file.resolve())
    finally:
        end_time = time.time()
        # cursor.close()
        # db_obj.close()
        
        '''print(f'\nTime elapsed: {timedelta(seconds=end_time - start_time)}')
        print('\nProgram completed.')
        print(f'\n\n{status}')
        counter_dict['total_size'] = counter_dict['first_size'] + counter_dict['second_size']
        counter_dict['first_size'] = str(humanize.naturalsize(counter_dict['first_size']))
        counter_dict['second_size'] = str(humanize.naturalsize(counter_dict['second_size']))
        counter_dict['total_size'] = str(humanize.naturalsize(counter_dict['total_size']))
        total_class['total'] = total_class['abnormal'] + total_class['normal']
        print(f'\n\n{counter_dict}')
        print(f'\n\n{total_class}')
        print(f'cycle_count: {cycle_count}')'''
        
        # ret_fn(f"good")
        # check and make call for specific operating system
        # _ = call('clear' if os.name == 'posix' else 'cls')
        print(return_file)
        sys.exit()
