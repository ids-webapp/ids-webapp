from pathlib import Path
import time
from dateutil.relativedelta import relativedelta
from datetime import timedelta, datetime, time
from scapy.all import *
from scapy.layers.l2 import Ether
import pickle
import mysql.connector
import pytz
import pprint as pp
import pandas as pd
import numpy as np
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import Pipeline
# My libraries
from signature import Signature
from db_conn import open_conn

# import signature


filter_info = [Signature("0: ICMP 192.168.56.101:any <> 192.168.56.103:any {'msg': '*'}"),
               Signature("2: TCP 192.168.56.101:8000 <> 192.168.56.103:![8080-9000] {'msg': 'yeah'}")]

ATTACKS = ['Normal', 'DoS', 'Probe', 'U2R', 'R2L']


# Get time relative for each packet
def get_packet_time_relative(packets):
    count = 0
    for pkt in packets:
        if count == 0:
            pkt.sent_time = '0.00'
        else:
            pkt.sent_time = str(pkt.time - packets[count - 1].time)

        count += 1

    return packets


# Filter packets functions
def filter_packets(a_pkt):
    tempDict = {'interface': None,
                'protocol': None,
                'src_ip': None,
                'src_port': None,
                # 'src_bytes': 0,
                'dest_ip': None,
                'dest_port': None,
                'packet_len': None,
                'frame_time': None,
                'arrival_datetime': None,
                'direction': None,
                'payload': None,
                'filter_id': None,
                'classification': None}

    if isinstance(a_pkt, Ether):
        if 'IP' in a_pkt:
            try:
                tempDict['protocol'] = a_pkt[2].name
                tempDict['src_ip'] = a_pkt[1].src
                tempDict['src_port'] = a_pkt[1].sport
                tempDict['dest_ip'] = a_pkt[1].dst
                tempDict['dest_port'] = a_pkt[1].dport
                tempDict['packet_len'] = len(a_pkt)
                tempDict['frame_time'] = a_pkt.sent_time

                temp_time = datetime.fromtimestamp(int(a_pkt.time))
                tempDict['arrival_datetime'] = temp_time.strftime('%Y-%m-%d %H:%M:%S')

                # if self.__machine_ip in tempDict['src_ip']:
                #     tempDict['direction'] = 'outbound'
                # elif self.__machine_ip in tempDict['dest_ip']:
                #     tempDict['direction'] = 'inbound'
                # else:
                tempDict['direction'] = 'unknown'

                tempDict['payload'] = str(
                    bytes(a_pkt[tempDict['protocol']].payload).decode(encoding='utf-8', errors='ignore'))
                # tempDict['payload'] = a_pkt.payload
            except AttributeError as ae:
                if 'ICMP' in a_pkt:
                    tempDict['protocol'] = a_pkt[2].name
                    tempDict['src_ip'] = a_pkt[1].src
                    # tempDict['src_port'] = 'any' # Not needed
                    tempDict['dest_ip'] = a_pkt[1].dst
                    # tempDict['dest_port'] = a_pkt[1].dport # Not needed
                    tempDict['packet_len'] = len(a_pkt)
                    tempDict['frame_time'] = a_pkt.sent_time

                    temp_time = datetime.fromtimestamp(int(a_pkt.time))
                    tempDict['arrival_datetime'] = temp_time.strftime('%Y-%m-%d %H:%M:%S')

                    # if self.__machine_ip in tempDict['src_ip']:
                    #     tempDict['direction'] = 'outbound'
                    # elif self.__machine_ip in tempDict['dest_ip']:
                    #     tempDict['direction'] = 'inbound'
                    # else:
                    tempDict['direction'] = 'unknown'

                    # tempDict['payload'] = bytes(a_pkt.payload).decode(encoding='ascii', errors='ignore')
                    tempDict['payload'] = str(
                        bytes(a_pkt[tempDict['protocol']].payload).decode(encoding='utf-8', errors='ignore'))
                else:
                    raise ValueError()
            except IndexError:
                raise ValueError()
                # pass
        else:
            # raise ValueError()
            pass

    return tempDict


# Try to load classification model
def load_model():
    model = None
    
    model_loc = Path.cwd() / f'fyp_pythonflask/models/knn_model.pkl'
    
    try:
        with open(model_loc, 'rb') as model_file:
            model = pickle.load(model_file)

    except Exception as e:
        print(e)
    return model


# Check packets against signatures
def check_packet_signature(pkt_dict):
    global filter_info
    health = []
    try:
        for idx, rule in enumerate(filter_info):
            for k, v in pkt_dict.items():
                # print(v['packets'])
                for p in v['packets']:
                    try:
                        pkt_sign = Signature(p)

                        if pkt_sign.is_equal_to_rule(rule):
                            p['filter_id'] = rule.s_id
                            # print(f'\n{rule.__repr__()} \n{pkt_sign.__repr__()}')
                            health.append('Unhealthy!')
                            # pass
                        # else:
                        #     health.append('healthy!')
                    except ValueError as ve:
                        # print(ve)
                        # print('wrong')
                        pass
                    except Exception as e:
                        print(e)
                        print('Cannot get signature from packet.')
    except Exception as err:
        print('Error getting signature.')
    # finally:
    # pp.pprint(pkt_dict)
    print('Number of bad packets: ', len(health))
    return pkt_dict


# Classify packets
def classify_live_packets(pkts):
    # Store formatted packets to list
    formatted_packets = []
    model = load_model()
    global ATTACKS
    temp_area = pkts

    # while True:
    #     if self._status['live_classification_status'] and len(self.live_packets['previous_packets']) > 0:
    print('In classifying live')
    try:
        # Get the previous packets
        # temp_area = self.live_packets['previous_packets'].pop(0)
        # print(len(temp_area))
        # pp.pprint(temp_area)
        if len(pkts) > 0:
            formatted_packets = []
            prepared_conn = []
            # Check signatures against packets
            # temp_area = check_packet_signature(pkts)

            # Iterate all the previous packets and add a classification
            for item, val in temp_area.items():
                prepared_conn.append([int(val['statistics']['duration']), val['statistics']['protocol'],
                                      val['statistics']['src_bytes'], val['statistics']['dest_bytes']])
                # for sub_item in val[item]:
                #     # sub_item['class'] = None
                #     # print(sub_item)
                #     formatted_packets.append(sub_item)

            # Model classification as follows
            preprocessed = pd.DataFrame(np.asarray(prepared_conn),
                                        columns=['duration', 'protocol_type', 'src_bytes', 'dst_bytes'])
            print(preprocessed)
            prediction_list = model.predict(preprocessed)
            print('Predictions: ', prediction_list)

            # Add the classification into the packets
            for idx, val in enumerate(prediction_list):
                try:
                    # Assign alert status to each stream or connection
                    if prediction_list[idx] > 0:
                        temp_area[idx]['alert'] = 1
                        # self.live_packets['alerts'].append(temp_area[idx]['statistics'])
                    else:
                        temp_area[idx]['alert'] = 0

                # for pkt in temp_area[idx]['packets']:
                #     pkt['classification'] = self.ATTACKS[prediction_list[idx]]

                    temp_area[idx]['statistics']['classification'] = ATTACKS[prediction_list[idx]]
                    # temp_area[idx]['statistics']['classification'] = 'DOs'
                except IndexError:
                    print('Index error.')
                except KeyError:
                    print('Key error.')

                # pass
            # print('Classification done.')
            # self.live_packets['prediction'].append(temp_area)
            print('Classification done.')

            # For debugging purposes only
            # if self.store_packets_to_db_thread.is_alive():
            #     print('store_db is still alive.')
    except Exception as e:
        print('damn')
        print(e)

    return temp_area


# Database connection
def get_conn():
    my_db = None
    
    try:
        my_db = open_conn()
    except mysql.connector.Error as mysql_err:
        print(mysql_err)
    else:
        try:
            if my_db.is_connected():
                db_info = my_db.get_server_info()
                print(f'Connected to database.')

                cursor = my_db.cursor()

                # Set global connection timeout arguments
                global_connect_timeout = f'SET GLOBAL connect_timeout=180'
                global_wait_timeout = f'SET GLOBAL connect_timeout=180'
                global_interactive_timeout = f'SET GLOBAL connect_timeout=180'

                cursor.execute(global_connect_timeout)
                cursor.execute(global_wait_timeout)
                cursor.execute(global_interactive_timeout)

                my_db.commit()
        except mysql.connector.Error as mysql_err:
            print(mysql_err)
        finally:
            cursor.close()
    
    return my_db

# Database connection pool
def get_conn_pool():
    db_pool = None

    try:
        my_db = open_conn()
    except mysql.connector.Error as mysql_err:
        print(mysql_err)
    else:
        try:
            if my_db.is_connected():
                db_info = my_db.get_server_info()
                print(f'Connected to database.')

                cursor = my_db.cursor()

                # Set global connection timeout arguments
                global_connect_timeout = f'SET GLOBAL connect_timeout=180'
                global_wait_timeout = f'SET GLOBAL connect_timeout=180'
                global_interactive_timeout = f'SET GLOBAL connect_timeout=180'

                cursor.execute(global_connect_timeout)
                cursor.execute(global_wait_timeout)
                cursor.execute(global_interactive_timeout)

                my_db.commit()
        except mysql.connector.Error as mysql_err2:
            print(mysql_err2)
        finally:
            cursor.close()

    return my_db


def store_alerts_to_db(data, db_conn=None):
    if not db_conn or (db_conn is None):
        raise Exception('Database object not found.')
    
    data_stor = data

    # Local timezone
    local_tz = pytz.timezone('Singapore')
    
    print('In storing alerts to db.')

    print('Length of check alerts: ', len(data))

    try:
        alert_query = ('insert into alerts '
                       '(protocol, src_ip, dest_ip, alert_datetime, classification, direction) '
                       'values (%s, %s, %s, %s, %s, %s)')

        add_values = ()

        # Datetime now
        # now = datetime.now(local_tz).replace(day=1).strftime('%Y-%m-%d %H:%M:%S')
        # now = now.replace(day=1)

        # a_db = self._db
        a_cursor = db_conn.cursor(prepared=True)

        for k, v in data_stor.items():
            if v['alert'] == 1:
                add_values = (v['statistics']['protocol'], v['statistics']['src_ip'],
                              v['statistics']['dest_ip'], v['statistics']['datetime'], v['statistics']['classification'],
                                v['statistics']['direction'])

                a_cursor.execute(alert_query, add_values)
                db_conn.commit()
                rowid = a_cursor.lastrowid
                affected_rows = a_cursor.rowcount

                # if affected_rows != -1:
                v['statistics']['alert_id'] = rowid
                v['statistics']['alert_datetime'] = v['statistics']['datetime']
                # data_stor['alerts'].append(v['statistics'])
                print('Alerts: ')
                pp.pprint(v['statistics'])
                print()
                print(f'Last insert id: {rowid}')
                print(f'Affected rows: {abs(affected_rows)}')
                    # pickle.dump(v['statistics'], self.log_file)
                    # self.log_file.write(
                    #     f"{str(v['statistics'])}\n")
                    # self.log_file.flush()
                    # return rowid
                # else:
                #     print('Add alerts to db error!')
            else:
                v['statistics']['alert_id'] = None
    except Exception as e:
        # self._status['store_alerts_to_db_status'] = False
        print(e)
    except mysql.connector.Error as mysql_err2:
        print(mysql_err2)
    finally:
        a_cursor.close()
        # db_conn.close()
        # print(f"Length of alerts: {len(self.live_packets['alerts'])}")
        # self.live_packets['store'].append(check_alerts)
        # print()
        # pp.pprint(check_alerts)
        # self._status['store_packets_to_db_status'] = True
        # self._status['store_alerts_to_db_status'] = False
    return data_stor


def store_packets_to_db(data, db_conn=None):
    if not db_conn or (db_conn is None):
        raise Exception('Database object not found.')

    data_stor = data

    # Local timezone
    local_tz = pytz.timezone('Singapore')
    
    print('In store packets to db')
    
    try:
        insert_query = ('insert into packets '
                        '(filter_id, interface, frame_time, src_ip, src_port, dest_ip, dest_port, protocol, ' +
                        'packet_length, classification, arrival_datetime, direction, alert_id) '
                        'values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)')
        add_values_list = []

        for k, v in data_stor.items():
            for p in v['packets']:
                add_tuple = (p['filter_id'], p['interface'], p['frame_time'], p['src_ip'], p['src_port'],
                                p['dest_ip'], p['dest_port'], p['protocol'], p['packet_len'],
                                v['statistics']['classification'], p['arrival_datetime'], p['direction'], v['statistics']['alert_id'])
                add_values_list.append(add_tuple)

        print('Length: ', len(add_values_list))
        # pp.pprint(add_values_list[-1])
        # a_db = self._db
        a_cursor = db_conn.cursor(prepared=True)
        a_cursor.executemany(insert_query, add_values_list)
        # rowid = a_cursor.lastrowid
        # affected_rows = a_cursor.rowcount

        # if affected_rows > 0:
        #     # return rowid
        #     print(rowid)
        # print(f'Affected rows: {affected_rows}')

        db_conn.commit()
        rowid = a_cursor.lastrowid
        affected_rows = a_cursor.rowcount
        # print(f'Affected rows: {a_cursor.rowcount}')
        # a_cursor.close()
        # print(f'Affected rows: {a_cursor.rowcount}')
        # self._status['store_packets_to_db_status'] = False

        # if affected_rows != -1:
        #     print(f'Last insert id: {rowid}')
        #     print(f'Affected rows: {abs(affected_rows)}')
        #     # return rowid
        # else:
        #     print('Insertion error!')
    except Exception as e:
        # self._status['store_packets_to_db_status'] = False
        # ALTER TABLE packets AUTO_INCREMENT = rowid
        print(e)
    except mysql.connector.Error as mysql_err2:
        print(mysql_err2)
    finally:
        a_cursor.close()
        # db_conn.close()
    
    return data_stor
