from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField,SubmitField, BooleanField
from wtforms.validators import DataRequired,Length,EqualTo,Email

class RegistrationForm(FlaskForm):
    username = StringField(label='Username',validators=[DataRequired(),Length(min=3,max=20)])
    email = StringField(label='Email',validators=[DataRequired(),Email()])
    utype = StringField(label='Utype',validators=[DataRequired()])
    password = PasswordField(label='Password',validators=[DataRequired(),Length(min=6,max=16)])
    confirm_password = PasswordField(label='Confirm Password',validators=[DataRequired(),EqualTo('password')])
    submit = SubmitField(label='Register')

class LoginForm(FlaskForm):
    email = StringField(label='Email',validators=[DataRequired(),Email()])
    password = PasswordField(label='Password', id='password',validators=[DataRequired(),Length(min=6,max=16)])
    show_password = BooleanField('Show password', id='check')
    submit = SubmitField(label='Login')
    submit2 = SubmitField(label='Register')

class ProfileForm(FlaskForm):
    oldpassword = PasswordField(label="Current Password",validators=[DataRequired(),Length(min=6,max=16)])
    newpassword = PasswordField(label="New Password",validators=[DataRequired(),Length(min=6,max=16)])
    confirmpassword = PasswordField(label="Repeat New Password",validators=[DataRequired(),EqualTo('newpassword')])
