from threading import Thread
import os, sys
import time

from flask import Flask,render_template,request
from flask_mysql_connector import MySQL, Params
from flask_bcrypt import Bcrypt
from fyp_pythonflask.models.sniffer import Sniffer

app=Flask(__name__)

app.config['SECRET_KEY']='thisisfirstflaskapp'
app.config[Params.MYSQL_HOST] = 'localhost'
app.config[Params.MYSQL_USER] = 'root'
app.config[Params.MYSQL_PASSWORD] = ''
app.config[Params.MYSQL_DATABASE] = 'csit321'
app.config['MAX_CONTENT_LENGTH'] = 10 * 1000 * 1000
app.config['UPLOAD_EXTENSIONS'] = ['.pcap']
app.config['UPLOAD_PATH'] = './fyp_pythonflask/pcap_uploads/'

mysql = MySQL(app)
bcrypt = Bcrypt(app)
sniff_obj = Sniffer(interface_name='enp0s8', machine_ip='192.168.56.101')

from fyp_pythonflask import routes

# if __name__ == "__main__":
#     # t1 = Thread(target=sniff_obj.init_thread)
#     # t1.daemon = True
#     # t1.start()
#     app.run(debug=True, threaded=False, use_reloader=False)
