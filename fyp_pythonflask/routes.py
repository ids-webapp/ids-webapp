import re
from pathlib import Path
import json, time
from datetime import datetime
from threading import Thread
import functools
# from flask_mysql_connector.flask_mysql_connector import MySQL
from werkzeug.utils import secure_filename
from fyp_pythonflask import app, mysql, bcrypt, sniff_obj
from flask import render_template, url_for, redirect, flash, request, session, jsonify, Response
from fyp_pythonflask.forms import RegistrationForm,LoginForm,ProfileForm
# from fyp_pythonflask.models import User
# from flask_mysql import MySQL


# Customized login_required decorator for protecting pages
# and loading user's workspace
def login_required(func):
    @functools.wraps(func)
    def secure_function(*args, **kwargs):
        if "loggedin" not in session:
            return redirect(url_for("login", next=request.url))
        return func(*args, **kwargs)

    return secure_function

#dashboard
@app.route('/home',methods=['POST','GET'])
@login_required
def homepage():
    return render_template('homepage.html', title='Home')

@app.route('/profile', methods=['POST', 'GET'])
@login_required
def profile():

    # To check the username of the current session
    if 'username' in session:
        username = session['username']
        #return username
        
    msg = {}
    change_ok = True
    db = mysql.connection

    form=ProfileForm()
    if request.method == 'POST' and 'oldpassword' in request.form and 'newpassword' in request.form and 'repeatpassword' in request.form:
        oldpassword = request.form['oldpassword']
        newpassword = request.form['newpassword']
        repeatpassword = request.form['repeatpassword']
        #return oldpassword

        mycursor = db.cursor(dictionary=True)
        query = f'select * from users where username = \'{username}\''
        mycursor.execute(query)
        account = mycursor.fetchone()

        if account:
            if not bcrypt.check_password_hash(account['hash_password'], oldpassword):
                msg['password_error'] = 'You have entered an invalid password! Please try again..'
                change_ok = False

            if not re.match(r'\b.{8,}\b', newpassword):
                msg['pwd_error'] = 'Password must be at least 8 characters!'
                change_ok = False

            if not newpassword == repeatpassword:
                msg['pwd_error2'] = 'Passwords do not match!'
                change_ok = False

            if bcrypt.check_password_hash(account['hash_password'], newpassword):
                msg['pwd_error3'] = 'You have recently used this password! Please try again..'
                change_ok = False

        if change_ok:
            # encrypt password
            hash_password = bcrypt.generate_password_hash(newpassword)

            query = """UPDATE users SET hash_password = %s WHERE username = %s"""
            value = (hash_password, username)
            mycursor.execute(query, value)

            affected_rows = mycursor.rowcount
            db.commit()
            mycursor.close()
            if (affected_rows == 1):
                msg['success'] = 'You have successfully changed your password!'
        

    return render_template('Profile.html', title='Profile', msg=msg, form=form)

@app.route('/datavisualization',methods=['POST','GET'])
@login_required
def datavisualization():
    return render_template('DV.html', title='Data Visualization')

@app.route('/rules')
@login_required
def rules():
    return render_template('Rules.html', title='Rules')

@app.route('/logout')
@login_required
def logout():
    # Remove session data, this will log the user out
    session.pop('loggedin', None)
    session.pop('username', None)
    # Return to login page
    return redirect(url_for('login'))

@app.route('/register',methods=['POST', 'GET'])
def register():

    msg = {}
    reg_ok = True
    db = mysql.connection
    # return render_template('Register.html', title='Register', msg=msg)
    form=RegistrationForm()
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form:
        username = request.form['username']
        hash_password = request.form['password']
        password2 =request.form['confirm_password']
        email = request.form['email']

    #elif request.method == 'POST':
        #msg = 'Please fill out the form!'
        mycursor = db.cursor(dictionary=True)

        # register_query = ('select username from users ' +\
        #                   'where username = %s')

        # register_values = (username)

        # register_query = ('insert into users (username, hash_password, email) '
        #                   'values (%s, %s, %s)')

        # register_values = (username)

        # mycursor.execute(register_query, register_values)
        query = f'select username from users where username = \'{username}\''
        # query = ('select username from users '
        #          'where username = %s')
        # value =(username, )

        mycursor.execute(query)

        account = mycursor.fetchone()
        # account = {
        #     'username': 'help',
        #     'password': '123456',
        #     'email': 'help@gmail.com'
        # }
        # account = {}
        if account:
            msg['username_error'] = 'Account already exists!'
            reg_ok = False

        # if not re.match(r'[^@]+@[^@]+\.[^@]+', email):
        if not re.match(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b', email):
            msg['email_error'] = 'Invalid email address!'
            reg_ok = False

        if not re.match(r'[A-z0-9]+', username):
            msg['username_error'] = 'Username must not contain special characters'
            reg_ok = False

        if not re.match(r'\b.{8,}\b', hash_password):
            msg['pwd_error'] = 'Password must be at least 8 characters!'
            reg_ok = False

        if not hash_password== password2:
            msg['pwd_error2'] = 'Passwords do not match!'
            reg_ok = False

        if not username or not hash_password or not email:
            msg = 'Please fill out the form!'
            reg_ok = False

        if reg_ok:
            # encrypt password
            hash_password = bcrypt.generate_password_hash(hash_password)

            mycursor.execute('INSERT INTO users (username, hash_password, email, is_active) VALUES (%s, %s, %s, %s)',
                             (username, hash_password, email, 'Yes'))

            affected_rows = mycursor.rowcount
            db.commit()
            mycursor.close()
            if (affected_rows == 1):
                msg['success'] = 'You have successfully registered!'
            # else:
            #     return False
            # msg['success'] = 'You have successfully registered!'

            # mycursor = mysql.connection.cursor(dictionary=True)

            # mycursor.execute("select * from global_priv")

            # account = mycursor.fetchone()
            # if account:
            #     session['loggedin'] = True
            #     session['username'] = account['username']
            #     return 'Logged in successfully!'
            # else:
            #     msg = 'Incorrect username/password!'
            #     return redirect(url_for('login'))

    return render_template('Register.html', title='Register', msg=msg, form=form)

@app.route('/db', methods=['GET', 'POST'])
def index2():
    from . import mysql

    print(mysql)

    mycursor = mysql.connection.cursor(dictionary=True)

    mycursor.execute("select * from global_priv")
    result = mycursor.fetchall()
    # Close the following after each transaction
    mycursor.close()
    return str(result)


    # Thashah's
    form=RegistrationForm()
    if form.validate_on_submit():
        user=User(username=form.username.data, email=form.email.data, password=form.password.data)
        db.session.add(user)
        db.session.commit()
        flash(f'Account created successfully for {form.username.data}', category='success')
        return redirect(url_for('login'))
    return render_template('Register.html', title='Register',form=form)

@app.route('/', methods=['POST','GET'])
@app.route('/login',methods=['POST','GET'])
def login():
    msg =''
    form=LoginForm()
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        username = request.form['username']
        password = request.form['password']
        next_url = request.form.get("next")


        mycursor = mysql.connection.cursor(dictionary=True)
        
        mycursor.execute('select * from users where username = %s',
                         (username, ))

        account = mycursor.fetchone()
        mycursor.close()
        if account and bcrypt.check_password_hash(account['hash_password'], password):

            session['loggedin'] = True
            session['username'] = account['username']
            
            # Redirect user to the page he wants
            if next_url:
                return redirect(next_url)
            
            # redirect user to homepage
            return redirect(url_for('homepage'))

        else:
            msg = 'Incorrect username/password!'

    return render_template('Login.html', title='Login',msg=msg, form=form)


# Get new live alerts
@app.route('/new_live_alerts')
def new_live_alerts():
    def get_live_alerts():
        json_data ={}
        try:
            # Infinite loop to get new live alerts
            while True:
                new_alerts = sniff_obj.get_available_live_alerts()
                
                if new_alerts:
                    json_data = json.dumps({
                        'time': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                        'live_alerts': new_alerts
                    })
                        
                    yield f'data:{json_data}\n\n'
                    
                # else:
                #     # yield f'data:{json_data}\n\n'
                time.sleep(1)
                #     continue
        finally:
            print("CLOSED!")
    t2 = Thread(target=get_live_alerts)
    t2.daemon = True
    t2.start()
    return Response(get_live_alerts(), mimetype='text/event-stream')


# Get total number of potential threats, 
# Also used for top attack type
@app.route('/get_total_potential_threats', methods=['POST', 'GET'])
def total_potential_threats():
    json_return_data = {}

    if request.method == 'POST':
        json_data = request.json
        if json_data['req_info'] == 'total_potential_threats':
            print(json_data['req_datetime'])

            # Get total potential_threats
            total_possible_threats = sniff_obj.get_total_potential_threats()
            json_return_data['data'] = total_possible_threats

    return jsonify(json_return_data)


# Get number of threats previous month
@app.route('/total_threats_prev_mth', methods=['POST', 'GET'])
def total_threats_prev_mth():
    json_return_data = {}
    
    if request.method == 'POST':
        json_data = request.json
        
        if json_data['req_info'] == 'total_threats_prev_mth':
            cleaned_date = json_data['req_datetime'].strip('\"')
        
            threats_prev_mth = sniff_obj.get_threats_prev_mth(**{'req_datetime': cleaned_date})
            json_return_data['data'] = threats_prev_mth
    
    return jsonify(json_return_data)


# Feed data for analysis
@app.route('/file-review')
@login_required
def file_review():
    return render_template('file-analysis.html', title='File Analysis')


# Upload file
@app.route('/file_upload_call', methods=['POST'])
def file_upload_call():
    Path(app.config['UPLOAD_PATH']).mkdir(mode=511,exist_ok=True)

    if "file-upload" in request.files:
        file_dict = dict(request.files)
        
        # Get file filename
        uploaded_file = file_dict['file-upload']
        filename = secure_filename(uploaded_file.filename)
        
        # Check secure_file is not empty
        if filename != '':
            file_ext = Path(filename).suffix
            
            if file_ext not in app.config['UPLOAD_EXTENSIONS']:
                return "Invalid file !", 400
            
            saved_path = Path(app.config['UPLOAD_PATH']) / filename
            uploaded_file.save(saved_path)

            response_dict = {'filename': str(saved_path.resolve()),
                             'msg': "Upload successful !"}
            return jsonify(response_dict), 200
        else:
            return "No file selected !", 400
        
    return "", 204


# File analysis function call to file capture in sniff
@app.route('/file_analysis', methods=['POST'])
def file_analysis():
    if request.method == "POST":
        json_data = request.json
        
        if json_data['req_info'] == 'file_analysis':
            # print(json_data['filename'])
            # print(Path.exists(Path(json_data['filename'])))
            stat = sniff_obj.start_file_capture(json_data['filename'])
            # print(f'file_status: {stat}')
            if stat != '':
                response_dict = {'filename': stat,
                                 'msg': "Analysis ok !"}
            else:
                response_dict = {}

            return jsonify(response_dict), 200

    return Response('Analysis failed'), 400


# File analysis function call to file capture in sniff
@app.route('/load_alert_log', methods=['POST'])
def load_alert_log():
    if request.method == "POST":
        json_data = request.json
        
        if json_data['req_info'] == 'load_alert_log':
            # print(f'In load alert log')
            # print(json_data['filename'])
            # print(Path.exists(Path(json_data['filename'])))

            data = sniff_obj.load_alerts_from_log(
                json_data['filename'], json_data['start'], json_data['end'])

            if len(data) != 0:
                response_dict = {'filename': json_data['filename'],
                                 'msg': "Load ok !",
                                 'data': data}
            elif len(data) == 0:
                response_dict = {'filename': json_data['filename'],
                                 'msg': "End of analysis"}
            else:
                response_dict = {'filename': json_data['filename'], 
                                 'msg': "Load failed !"}
                return jsonify(response_dict), 400

            return jsonify(response_dict), 200

    return Response('Load data failed'), 400


# HTTP Error handler for rejecting large files
@app.errorhandler(413)
def too_large(e):
    return "File must be smaller than 10MB !", 413


@app.route('/get_alerts_dist', methods=['POST', 'GET'])
def get_alerts_dist():
    json_return_data = {}
    
    if request.method == "POST":
        json_data = request.json
        
        if json_data['req_info'] == 'alerts_dist':
            print(f'In alerts dist')
            
            data = sniff_obj.get_alerts_dist()
            
            json_return_data['data'] = data
            
            return jsonify(json_return_data), 200
        
    return jsonify(json_return_data), 400


@app.route('/get_alerts_protocol_dist', methods=['POST', 'GET'])
def get_alerts_protocol_dist():
    json_return_data = {}

    if request.method == "POST":
        json_data = request.json

        if json_data['req_info'] == 'alerts_protocol_dist':
            print(f'In alerts dist')

            data = sniff_obj.get_alerts_protocol_dist()

            json_return_data['data'] = data

            return jsonify(json_return_data), 200

    return jsonify(json_return_data), 400

# Debugging purposes only
