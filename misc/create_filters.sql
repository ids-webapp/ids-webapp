CREATE TABLE filters(  
    filter_id           BIGINT NOT NULL AUTO_INCREMENT comment 'primary key',
    filter_args         varchar(1024) not null,
    modified_by         varchar(50) not null COMMENT 'modified_by',
    datetime_modified   DATETIME null COMMENT 'datetime_modified',
    constraint FILTERS_PK   PRIMARY KEY (filter_id),
    constraint FILTERS_FK   FOREIGN KEY (modified_by)
                            REFERENCES users(username)
) default charset utf8 comment '';