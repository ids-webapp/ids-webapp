CREATE TABLE users(  
    username        varchar(50) NOT NULL primary key comment 'primary key',
    first_name      varchar(100) not null,
    last_name       varchar(100) not null,
    hash_password   varchar(255),
    email           varchar(255),
    last_login      DATETIME null COMMENT 'last_login',
    last_logout     DATETIME null COMMENT 'last_logout',
    constraint USERS_PK     PRIMARY KEY (username)
) default charset utf8 comment '';