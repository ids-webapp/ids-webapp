import mysql.connector

cnx = mysql.connector.connect(
    host="localhost",
    user="csit115",
    password="csit115",
    database="csit321"
)

print(cnx)

mycursor = cnx.cursor()
# mycursor = cnx.cursor()

mycursor.execute("show databases")
# mycursor.execute("use csit321")
result = mycursor.fetchall()
# print(result)
mycursor.execute("show tables")

result = mycursor.fetchall()
print(result)
mycursor.close()
cnx.close()
