CREATE TABLE alerts(  
    alert_id            BIGINT NOT NULL AUTO_INCREMENT comment 'primary key',
    filter_id           BIGINT NOT NULL,
    packet_id           BIGINT NOT NULL,
    filter_args         varchar(1024) not null,
    src_ip              char(15) null,
    dest_ip             char(15) null,
    arrival_datetime    DATETIME null COMMENT 'arrival_datetime',
    protocol            char(5) not null,
    classification      varchar(255) not null,
    constraint ALERTS_PK   PRIMARY KEY (alert_id),
    constraint ALERTS_FK   FOREIGN KEY (filter_id)
                           REFERENCES filters(filter_id),
    constraint ALERTS_FK2  FOREIGN KEY (packet_id)
                           REFERENCES packets(packet_id)
) default charset utf8 comment '';