CREATE TABLE packets(  
    packet_id           BIGINT NOT NULL AUTO_INCREMENT comment 'primary key',
    filter_id           BIGINT NOT NULL,
    inteface            varchar(50) not null,
    frame_time          varchar(20) null,
    src_ip              char(15) null,
    src_port            char(5) null,
    dest_ip             char(15) null,
    dest_port           char(5) null,
    protocol            char(5) not null,
    packet_length       varchar(255) not null,
    classification      varchar(255) not null,
    arrival_datetime    DATETIME null COMMENT 'arrival_datetime',
    constraint PACKETS_PK   PRIMARY KEY (packet_id),
    constraint PACKETS_FK   FOREIGN KEY (filter_id)
                            REFERENCES filters(filter_id)
) default charset utf8 comment '';