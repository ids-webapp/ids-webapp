# An analytics web app for Intrusion Detection System

This project aims to create an analytics web app with PyShark.

## Requirements

1. Wireshark and tshark must be installed in the machine (For linux OS).

# Misc
You can look around to get an idea how to structure your project and, when done, you can safely delete this project.

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)
