from threading import Thread
import os
import sys
import time
# from webapp import create_app
# app = create_app()
from fyp_pythonflask import app, sniff_obj

def start_up():
    t1 = Thread(target=sniff_obj.init_thread)
    t1.daemon = True
    t1.start()



#Checks if the run.py file has executed directly and not imported
if __name__ == '__main__':
    start_up()
    app.run(debug=True, threaded=True, use_reloader=False)